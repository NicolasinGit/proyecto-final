import csv

def ExisteCodigo(codigo):
    with open('Inventario.csv') as File:
        reader=csv.DictReader(File)
        for row in reader:
            if(codigo==row['codigo']):
                return row
        return "No existe"
